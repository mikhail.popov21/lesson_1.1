# Lesson 1.1


## Описание задания
```sh
Задача: написать докер композ, в котором будет 3 сервиса:
- nginx
- postgresql
- app (само приложение) (докер файл с инсталяцией зависимостей) 
nginx port 8000:8000
Приложения джанго 
https://gitlab.com/chumkaska1/django_blog.git
Создать нетворк для них
```
#### Структура проекта:
````bash
|-- django_blog
|   |-- Blog
|   |   |-- __init__.py
|   |   |-- settings.py
|   |   `-- urls.py
|   |-- Dockerfile
|   |-- __init__.py
|   |-- article
|   |   |-- __init__.py
|   |   |-- admin.py
|   |   |-- apps.py
|   |   |-- forms.py
|   |   |-- migrations
|   |   |   |-- 0001_initial.py
|   |   |   `-- __init__.py
|   |   |-- models.py
|   |   |-- tests.py
|   |   |-- urls.py
|   |   `-- views.py
|   |-- asgi.py
|   |-- config
|   |   `-- nginx
|   |       `-- django.conf
|   |-- manage.py
|   |-- requirements.txt
|   |-- settings.py
|   |-- templates
|   |   |-- detail.html
|   |   |-- edit_page.html
|   |   |-- index.html
|   |   |-- login.html
|   |   `-- register_page.html
|   |-- urls.py
|   `-- wsgi.py
`-- docker-compose.yml

````

## 1. Description Docker-compose:
 В рамках данного задания необходимо создать файл docker-compose.yaml. С помощью Docker-compose возможно запустить кластер приложений, в котором будет обеспечена их взаимодействие и работоспособность. Ниже привидено подробное описание каждого сервиса. 


## 2. Description db-service:
 В рамках данного задания необходимо установить базу данных Postgres. Ниже представлен "блок" отвечающий за установку базы данных. 
 ```sh
 db:
    image: postgres
    restart: always
    container_name: db
    env_file:
      - django_blog/.env
    expose:
      - "5432"
    volumes:
      - pg_data:/var/lib/postgresql/data/
    networks:
      - conn
 ```
- ```image``` -- указывает название образа и версию, поскольку версия не указана, докер возьмет самую крайнюю версию из DockerHub.
- ```restart```-- указывает на то, что в случае неккоретного запуска будет перезапускаться постоянно.
- ```container_name``` -- название контейнера
- ```env_file``` -- файл с конфиграцией базы данных, в случае чего можно указать данные из этого файла в отдельном подблоке под названием ```environment```.
- ```expose``` -- открывает порты для приложения
- ```volumes``` -- указывает том для монтирования
- ```networks``` -- объединяет контейнеры по сети в данном случае делаем ```bridge```.

## 3. Description web-service:
```sh
web:
    build: django_blog/.
    restart: on-failure
    container_name: web
    expose:
      - "8000"
    command: bash -c "python manage.py makemigrations && python manage.py migrate && gunicorn wsgi:application -w 2 -b :8000"
    volumes:
      - web_django:/usr/src/app/static
    depends_on:
      - db
 ```
- ```build```-- указывает место расположения DockerFile относительно пути расположения файла docker-compose. 
- ```restart: on-failure``` -- указаывает на перезапуск контейнера в случае если он исполнился с ошибкой. 
- ```expose:8000```  -- открывает порт на 8000 порту, поскольку на данном порту будет работать приложение. 
```command: bash -c "python manage.py makemigrations && python manage.py migrate && gunicorn wsgi:application  -b :8000"``` -- команда запускает наше приложение тремя командами. Первая команда ```python manage.py makemigrations``` создает новые миграции базы данных(БД).
Вторая команда ```python manage.py migrate``` применяет миграции БД.
Третья команда ```gunicorn wsgi:application -b :8000"``` поднимает приложение на localhost на 8000 порту.  
```depends_on:``` -- указывает зависимости между сервисами  в данном случае с сервисом базы данных. 
### 3.1. Short description Dockerfile for web-service
```sh 
FROM python:3.9
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt
COPY . .
```
- Установка python версии 3.9
- Установка пакетного менеджера python "pip" и его обновление
- Установка требующихся пакетов из requirements.txt
- Копирование проекта

## 4. Description nginx-service:
``` sh
nginx:
    image: nginx:latest
    restart: on-failure
    container_name: nginx
    ports:
      - "8000:8000"
    volumes:
      - ./django_blog/config/nginx/django.conf:/etc/nginx/conf.d/default.conf
    depends_on:
      - web
    networks:
      - conn
```

- ```ports```-- октрывает порты по которым будет рабоать приложение.
- ```volumes``` -- в данном сервисе указывается путь монтирования конфигурационного файла относительно файла docker-compose 
- ```depends_on:``` -- указывает зависимость с приложением Django.

## 5. Building and check of work
- Сборка docker-compase производится следующей командой:
```docker-compose build```
- Запуск docker-compose  производится следующей командой:
```docker-compose up``` или в фоновом режиме ```docker-compose up -d ```



 ##### Проверка работы :
- для проверки работы необходимо выполнить команду ```curl 127.0.0.1:8000```  В случае если настройка выполнена корректно отрисуется страница в виде html, а в логах можно увидеть подобную запись:
```92.168.16.1 - - [12/Dec/2021:19:31:33 +0000] "GET / HTTP/1.1" 200 2084 "-" "curl/7.61.1" "-"``` 
